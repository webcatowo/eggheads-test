# eggheads-test

sql запросы  - src/sql

тестовый код на php js - src/TestCode

Ответ на первый вопрос:

Опишите, какие проблемы могут возникнуть при использовании данного кода
   ...
   $mysqli = new mysqli("localhost", "my_user", "my_password", "world");
   $id = $_GET['id'];
   $res = $mysqli->query('SELECT * FROM users WHERE u_id='. $id);
   $user = $res->fetch_assoc();
   ...

Злоумышлиник может выполнить любой sql запрос. Проблема в том что мы ни как обрабатываем входящие данные и не используем параметризированые запросы.