function printOrderTotal(responseString) {
    const responseJSON = JSON.parse(responseString);

    if (!Array.isArray(responseJSON)) {
        console.error('Non expected format');
        return;
    }

    const total = responseJSON.reduce(
        (total, item) => item.hasOwnProperty('price') ?  total + parseInt(item.price) : total,
        0,
    );

    let totalString = total <= 0 ? "Бесплатно" : `${total} руб`

    console.log(`Стоимость заказа: ${totalString}`);
}

printOrderTotal('[{"price": 1},{"price": 1},{}]');
