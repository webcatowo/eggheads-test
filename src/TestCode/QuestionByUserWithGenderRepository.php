<?php

namespace TestCode;

use mysqli;
use mysqli_result;
use mysqli_stmt;

class QuestionByUserWithGenderRepository
{

    /*
     * По идеи эти данные должны быть у моделей/сущностей, конфигов, ну сохраню в констаты в избежании магических значений
     */
    private const DB_HOST = 'mysql';
    private const DB_NAME_KEY = 'MYSQL_DATABASE';
    private const DB_USER_KEY = 'MYSQL_USER';
    private const DB_PASSWORD_KEY = 'MYSQL_PASSWORD';
    private const QUESTIONS_TABLE_NAME = 'questions';
    private const QUESTIONS_FIELD_ID = 'id';
    private const QUESTIONS_FIELD_USER_ID = 'user_id';
    private const QUESTIONS_FIELD_CATALOG_ID = 'catalog_id';
    private const USERS_TABLE_NAME = 'users';
    private const USERS_FIELD_ID = 'id';
    private const USERS_FIELD_NAME = 'name';
    private const USERS_FIELD_GENDER = 'gender';
    private mysqli $connection;


    public function __construct(
        ?mysqli $connection = null
    ) {
        $this->connection = $connection ?? new mysqli(
            self::DB_HOST,
            $_ENV[self::DB_USER_KEY],
            $_ENV[self::DB_PASSWORD_KEY],
            $_ENV[self::DB_NAME_KEY],
        );
    }


    public function getConnection(): mysqli
    {
        return $this->connection;
    }


    public function getQuestionsWithUserGenderByCatalogId(int $catalogId): array
    {
        $query = $this->getQuery($catalogId);
        $result = [];

        while ($row = $query->fetch_assoc()) {
            $result[] = [
                'question' => array_filter(
                    $row,
                    function ($key) {
                        return strpos(self::QUESTIONS_TABLE_NAME, $key) !== false;
                    },
                    ARRAY_FILTER_USE_KEY
                ),
                'user' => array_filter(
                    $row,
                    function ($key) {
                        return strpos(self::USERS_TABLE_NAME, $key) !== false;
                    },
                    ARRAY_FILTER_USE_KEY
                ),
            ];
        }

        return $result;
    }


    /**
     * @description return sql string, expected one int param
     */
    private function getStmt(): mysqli_stmt
    {
        $questionTableName = self::QUESTIONS_TABLE_NAME;
        $questionUserId = self::QUESTIONS_FIELD_USER_ID;
        $questionFieldCatalogId = self::QUESTIONS_FIELD_CATALOG_ID;
        $userTableName = self::USERS_TABLE_NAME;
        $userFieldId = self::USERS_FIELD_ID;
        $userFieldName = self::USERS_FIELD_NAME;
        $userFieldGender = self::USERS_FIELD_GENDER;

        $sql = <<<SQL
SELECT *, {$userTableName}.{$userFieldName}, {$userTableName}.{$userFieldGender} 
FROM {$questionTableName} 
LEFT JOIN {$userTableName} ON {$userTableName}.{$userFieldId} = {$questionTableName}.{$questionUserId}
WHERE {$questionTableName}.{$questionFieldCatalogId} = ?
SQL;

        $stmt = $this->getConnection()
            ->prepare($sql);

        if (!$stmt) {
            throw new \Exception('Failed to prepare statement');
        }

        return $stmt;
    }

    private function getQuery(int $catalogId): mysqli_result
    {
        $stmt = $this->getStmt();
        $stmt->bind_param('i', $catalogId);
        $stmt->execute();
        $query = $stmt->get_result();
        $stmt->close();

        if (!$query) {
            throw new \Exception('Failed execute statement');
        }

        return $query;
    }
}