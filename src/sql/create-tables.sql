CREATE TABLE users
(
    id      INT NOT NULL AUTO_INCREMENT,
    name    VARCHAR(255),
    phone   VARCHAR(255),
    email   VARCHAR(255),
    gender  INT,
    created DATE,
    PRIMARY KEY (id)
);

CREATE TABLE orders
(
    id       INT NOT NULL AUTO_INCREMENT,
    subtotal VARCHAR(255),
    created  DATE,
    city_id  VARCHAR(255),
    user_id  VARCHAR(255),
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES users (id)
);

CREATE TABLE questions
(
    id         INT NOT NULL AUTO_INCREMENT,
    user_id    INT,
    catalog_id INT,
    created    DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES users (id)
);

CREATE TABLE employees
(
    id            INT NOT NULL AUTO_INCREMENT,
    name          VARCHAR(255),
    last_name     VARCHAR(255),
    department_id INT,
    salary        INT,
    PRIMARY KEY (id)
);