with last_date as (select o.user_id, MAX(o.created) last_date from orders o group by o.user_id)

select
    u.name,
    u.phone,
    SUM(o.subtotal),
    AVG(o.subtotal)
from users u
         join eggheads.orders o on u.id = o.user_id
         join last_date ld on ld.user_id = u.id
group by u.name, u.phone
;